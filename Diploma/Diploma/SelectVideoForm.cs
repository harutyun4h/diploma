using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace Diploma
{
    public partial class SelectVideoForm : Form
    {
        private bool ExitThread = false;
        private ToolStripItem Binarise;
        private ToolStripItem ConvertIntoMatrix;
        private ToolStripItem Equalize;
        private ToolStripItem SaveGreyScale8Bit;
        private ToolStripItem BinariseOtsu;
        private ToolStripItem LinearContrast;
        private Tools _tools = new Tools();
        private int start;
        private int end;

        public SelectVideoForm()
        {
            InitializeComponent();

            this.FileMenu.Menu = new System.Windows.Forms.ContextMenuStrip();
            this.FileMenu.Menu.Items.Add("Open", null, BrowseVideoButton_Click);
            this.FileMenu.Menu.Items.Add("Select export folder", null, selectExportFolder);
            this.pictureBox1.Image = Image.FromFile("logo_am.png");
            this.Icon = new Icon("logo_am.ico");

            this.ToolsMenu.Menu = new System.Windows.Forms.ContextMenuStrip();

            Binarise = this.ToolsMenu.Menu.Items.Add("Binarise - Simple", null, ToolStripItemClicked);
            LinearContrast = this.ToolsMenu.Menu.Items.Add("Linear Contrast", null, ToolStripItemClicked);
            BinariseOtsu = this.ToolsMenu.Menu.Items.Add("Binarise By Otsu", null, ToolStripItemClicked);
            ConvertIntoMatrix = this.ToolsMenu.Menu.Items.Add("Convert Into Matrix", null, ToolStripItemClicked);
            Equalize = this.ToolsMenu.Menu.Items.Add("Equalize", null, ToolStripItemClicked);
            SaveGreyScale8Bit = this.ToolsMenu.Menu.Items.Add("Save GreyScale 8Bit", null, ToolStripItemClicked);
        }

        private string mp4FilePath;
        private string outputPath = @"C:\output";

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Click on the link below to continue learning how to build a desktop app using WinForms!
            System.Diagnostics.Process.Start("http://aka.ms/dotnet-get-started-desktop");

        }

        private async void BrowseVideoButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                mp4FilePath = choofdlog.FileName;
                //string[] arrAllFiles = choofdlog.FileNames; //used when Multiselect = true 
                this.ExecuteFramesButton.Enabled = true;
                this.numericUpDown1.Enabled = true;
                this.numericUpDown2.Enabled = true;
            }
        }

        private async void ExecuteFramesButton_Click(object sender, EventArgs e)
        {
            this.ProgressBar.Enabled = true;
            this.ProgressBar.Maximum = 100;
            this.ProgressBar.Step = 1;
            var task = Task.Run(async () => await ExecuteFrames());
        }

        private async Task ExecuteFrames()
        {
            if (end == start)
            {
                return;
            }
            if (this.ExecuteFramesButton.Text.Equals("Stop"))
            {
                Invoke(new Action(() =>
                {
                    ExitThread = true;
                }));
                return;
            }
            Invoke(new Action(() =>
            {
                this.FileMenu.Enabled = false;
                this.ToolsMenu.Enabled = false;
                this.numericUpDown1.Enabled = false;
                this.numericUpDown2.Enabled = false;
                this.ExecuteFramesButton.Text = "Stop";
            }));

            using (var engine = new Engine())
            {
                var mp4 = new MediaFile { Filename = mp4FilePath };
                engine.GetMetadata(mp4);
                if (start > end)
                {
                    var temp = end;
                    end = start;
                    start = temp;
                }

                float fpsInterval = (float)1000 / (float)mp4.Metadata.VideoData.Fps;

                var i = start * fpsInterval;
                Invoke(new Action(() =>
                   {
                       this.ProgressBar.Maximum = (int)(end * fpsInterval - start * fpsInterval);
                   }));

                while (i < (mp4.Metadata.Duration.Seconds + 60 * mp4.Metadata.Duration.Minutes) * mp4.Metadata.VideoData.Fps && i < end * fpsInterval)
                {
                    if (ExitThread)
                    {
                        Invoke(new Action(() =>
                        {
                            ExitThread = false;
                            this.ProgressBar.Value = 0;
                            this.ExecuteFramesButton.Text = "Start";
                            this.FileMenu.Enabled = true;
                            this.ToolsMenu.Enabled = true;
                            this.numericUpDown1.Enabled = true;
                            this.numericUpDown2.Enabled = true;
                            this.ExecuteFramesButton.Enabled = true;
                        }));
                        return;
                    }
                    var options = new ConversionOptions
                    {
                        Seek = TimeSpan.FromMilliseconds(i * fpsInterval),
                        VideoFps = (int)mp4.Metadata.VideoData.Fps,
                    };
                    string imageInfo = string.Format("{0}\\image-{1}-", outputPath, i, "O.bmp");
                    var outputFile = new MediaFile() { Filename = imageInfo + "O.bmp" };

                    engine.GetThumbnail(mp4, outputFile, options);
                    _tools.SetFIleLink(imageInfo);
                    GetBitMap(imageInfo);
                    i++;
                    Invoke(new Action(() =>
                    {
                        this.ProgressBar.Value = this.ProgressBar.Value < this.ProgressBar.Maximum ? this.ProgressBar.Value + 1 : this.ProgressBar.Maximum;
                    }));
                }

            }
            Invoke(new Action(() =>
            {
                this.FileMenu.Enabled = true;
                this.ToolsMenu.Enabled = true;
                this.numericUpDown1.Enabled = true;
                this.numericUpDown2.Enabled = true;
                this.ExecuteFramesButton.Enabled = true;
                this.ProgressBar.Value = 0;
                this.ExecuteFramesButton.Text = "Start";
            }));
        }

        private void GetBitMap(string fileLink)
        {
            Bitmap bmp = (Bitmap)Image.FromFile(fileLink + "O.bmp");
            var greyScaleImage = new Bitmap(bmp.Width, bmp.Height);

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int x = 0; x < bmp.Height; x++)
                {
                    Color oc = bmp.GetPixel(i, x);
                    int grayScale = (int)((oc.R * 0.3) + (oc.G * 0.59) + (oc.B * 0.11));
                    Color nc = Color.FromArgb(oc.A, grayScale, grayScale, grayScale);
                    greyScaleImage.SetPixel(i, x, nc);
                }
            }
            greyScaleImage = greyScaleImage.Clone(new Rectangle(0, 0, greyScaleImage.Width, greyScaleImage.Height), PixelFormat.Format8bppIndexed);

            #region
            ////get image dimension
            //int width = bmp.Width;
            //int height = bmp.Height;

            ////3 bitmap for red green blue image
            //Bitmap rbmp = new Bitmap(bmp);
            //Bitmap gbmp = new Bitmap(bmp);
            //Bitmap bbmp = new Bitmap(bmp);

            ////red green blue image
            //for (int y = 0; y < height; y++)
            //{
            //    for (int x = 0; x < width; x++)
            //    {
            //        //get pixel value
            //        Color p = bmp.GetPixel(x, y);

            //        //extract ARGB value from p
            //        int a = p.A;
            //        int r = p.R;
            //        int g = p.G;
            //        int b = p.B;

            //        //set red image pixel
            //        rbmp.SetPixel(x, y, Color.FromArgb(a, r, 0, 0));


            //        gbmp.SetPixel(x, y, Color.FromArgb(a, 0, g, 0));

            //        //set blue image pixel
            //        bbmp.SetPixel(x, y, Color.FromArgb(a, 0, 0, b));

            //    }
            //}

            ////write (save) red image
            //rbmp.Save(fileLink + "R.bmp");

            ////write(save) green image
            //gbmp.Save(fileLink + "G.bmp");

            ////write (save) blue image
            //bbmp.Save(fileLink + "B.bmp");


            //clone.Save(fileLink + "test.bmp");
            #endregion


            if (((ToolStripMenuItem)this.SaveGreyScale8Bit).Checked)
            {
                _tools.SaveGreyScale8Bit((Bitmap)greyScaleImage.Clone());
            }
            if (((ToolStripMenuItem)this.Binarise).Checked)
            {
                _tools.BitmapToBlackWhite((Bitmap)bmp.Clone());
            }
            if (((ToolStripMenuItem)this.BinariseOtsu).Checked)
            {
                _tools.OtsuBinarization((Bitmap)bmp.Clone());
            }
            if (((ToolStripMenuItem)this.ConvertIntoMatrix).Checked)
            {
                _tools.SaveMatrixInTxtFile((Bitmap)greyScaleImage.Clone());
            }
            if (((ToolStripMenuItem)this.Equalize).Checked)
            {
                _tools.HistEqualization((Bitmap)bmp.Clone());
            }
            if (((ToolStripMenuItem)this.LinearContrast).Checked)
            {
                _tools.Contrastation((Bitmap)bmp.Clone());
            }
            #region
            // Image m = ToolStripRenderer.CreateDisabledImage(clone);
            // m.Save(fileLink + "greyscaleIMAGECLASS.bmp");
            #endregion
        }

        private void selectExportFolder(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    outputPath = fbd.SelectedPath;
                }
            }
        }
        private void ToolStripItemClicked(object sender, EventArgs e)
        {
            ((ToolStripMenuItem)sender).Checked = !((ToolStripMenuItem)sender).Checked;
        }

        private void PSNRComparison_Click(object sender, EventArgs e)
        {
            PSNRForm psnrForm = new PSNRForm(); // Instantiate a Form3 object.
            psnrForm.Show(); // Show Form3 and
        }
        

        private void numericUpDown1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void numericUpDown2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }


        private void numericUpDown2_KeyUp(object sender, KeyEventArgs e)
        {

            this.end = (int)numericUpDown2.Value;
        }

        private void numericUpDown1_KeyUp(object sender, KeyEventArgs e)
        {
            this.start = (int)numericUpDown1.Value;
        }
    }
}
