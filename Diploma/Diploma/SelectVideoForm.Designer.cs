using Diploma.Menus;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Diploma
{
    partial class SelectVideoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        /// 
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// private CheckBox Binarise;
       

        private void InitializeComponent()
        {
            this.ExecuteFramesButton = new System.Windows.Forms.Button();
            this.PSNRComparison = new System.Windows.Forms.Button();
            this.StartTimeLabel = new System.Windows.Forms.Label();
            this.EndTimeLabel = new System.Windows.Forms.Label();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.FileMenu = new Diploma.Menus.MenuButton();
            this.ToolsMenu = new Diploma.Menus.MenuButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ExecuteFramesButton
            // 
            this.ExecuteFramesButton.Enabled = false;
            this.ExecuteFramesButton.Location = new System.Drawing.Point(538, 274);
            this.ExecuteFramesButton.Name = "ExecuteFramesButton";
            this.ExecuteFramesButton.Size = new System.Drawing.Size(174, 49);
            this.ExecuteFramesButton.TabIndex = 0;
            this.ExecuteFramesButton.Text = "Start";
            this.ExecuteFramesButton.Click += new System.EventHandler(this.ExecuteFramesButton_Click);
            // 
            // PSNRComparison
            // 
            this.PSNRComparison.Location = new System.Drawing.Point(189, 1);
            this.PSNRComparison.Name = "PSNRComparison";
            this.PSNRComparison.Size = new System.Drawing.Size(165, 29);
            this.PSNRComparison.TabIndex = 21;
            this.PSNRComparison.Text = "PSNR Comparison";
            this.PSNRComparison.Click += new System.EventHandler(this.PSNRComparison_Click);
            // 
            // StartTimeLabel
            // 
            this.StartTimeLabel.Location = new System.Drawing.Point(30, 249);
            this.StartTimeLabel.Name = "StartTimeLabel";
            this.StartTimeLabel.Size = new System.Drawing.Size(186, 23);
            this.StartTimeLabel.TabIndex = 10;
            this.StartTimeLabel.Text = "Start processing from (sec)";
            // 
            // EndTimeLabel
            // 
            this.EndTimeLabel.Location = new System.Drawing.Point(306, 249);
            this.EndTimeLabel.Name = "EndTimeLabel";
            this.EndTimeLabel.Size = new System.Drawing.Size(177, 23);
            this.EndTimeLabel.TabIndex = 11;
            this.EndTimeLabel.Text = "End processing from (sec)";
            // 
            // ProgressBar
            // 
            this.ProgressBar.Enabled = false;
            this.ProgressBar.Location = new System.Drawing.Point(12, 172);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(700, 23);
            this.ProgressBar.TabIndex = 12;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Enabled = false;
            this.numericUpDown1.Location = new System.Drawing.Point(33, 290);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 24;
            this.numericUpDown1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDown1_KeyPress);
            this.numericUpDown1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDown1_KeyUp);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Enabled = false;
            this.numericUpDown2.Location = new System.Drawing.Point(309, 290);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown2.TabIndex = 25;
            this.numericUpDown2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDown2_KeyPress);
            this.numericUpDown2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDown2_KeyUp);
            // 
            // FileMenu
            // 
            this.FileMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.FileMenu.Location = new System.Drawing.Point(1, 1);
            this.FileMenu.Name = "FileMenu";
            this.FileMenu.Size = new System.Drawing.Size(88, 29);
            this.FileMenu.TabIndex = 20;
            this.FileMenu.Text = "File";
            // 
            // ToolsMenu
            // 
            this.ToolsMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ToolsMenu.Location = new System.Drawing.Point(95, 1);
            this.ToolsMenu.Name = "ToolsMenu";
            this.ToolsMenu.Size = new System.Drawing.Size(88, 29);
            this.ToolsMenu.TabIndex = 20;
            this.ToolsMenu.Text = "Tools";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(457, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(255, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // SelectVideoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 366);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.numericUpDown2);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.ExecuteFramesButton);
            this.Controls.Add(this.StartTimeLabel);
            this.Controls.Add(this.EndTimeLabel);
            this.Controls.Add(this.ProgressBar);
            this.Controls.Add(this.FileMenu);
            this.Controls.Add(this.ToolsMenu);
            this.Controls.Add(this.PSNRComparison);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SelectVideo";
            this.Text = "Diploma - Main";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Button ExecuteFramesButton;

        private Label StartTimeLabel;
        private Label EndTimeLabel;

        private ProgressBar ProgressBar;
        private MenuButton FileMenu;
        private MenuButton ToolsMenu;
        private Button PSNRComparison;
        private NumericUpDown numericUpDown1;
        private NumericUpDown numericUpDown2;
        private PictureBox pictureBox1;
    }
}

