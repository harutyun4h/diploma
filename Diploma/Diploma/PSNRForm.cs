﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Diploma
{
    public class PSNRForm : Form
    {
        public PSNRForm()
        {
            InitializeComponent();
            this.Icon = new Icon("logo_am.ico");
            this.pictureBox1.Image = Image.FromFile("default.png");
            this.pictureBox2.Image = Image.FromFile("default.png");
        }
        private PictureBox pictureBox2;
        private Label label1;
        private TextBox textBox1;
        private Button button1;
        private Button button2;
        private Button button3;
        private PictureBox pictureBox1;
        private string originalFileLink;
        private string testFileLink;

        private void InitializeComponent()
        {
            this.Text = "Diploma - Comparison";
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(291, 243);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(370, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(313, 243);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 376);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "PSNR value:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(112, 373);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(80, 261);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(132, 40);
            this.button1.TabIndex = 4;
            this.button1.Text = "Select Original Image";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.SelectOriginalImage);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(493, 261);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(121, 40);
            this.button2.TabIndex = 5;
            this.button2.Text = "Select Test Image";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.SelectTestImage);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(608, 376);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 6;
            this.button3.Text = "Compare";
            this.button3.Enabled = false;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Compare);
            // 
            // PSNRForm
            // 
            this.ClientSize = new System.Drawing.Size(695, 409);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "PSNRForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private void Compare(object sender, EventArgs e)
        {
            using (MagickImage image1 = new MagickImage(originalFileLink))
            {
                using (MagickImage image2 = new MagickImage(testFileLink))
                {
                    double distortion = image1.Compare(image2, ErrorMetric.PeakSignalToNoiseRatio);
                    this.textBox1.Text = distortion.ToString();
                }
            }
        }

        private void SelectTestImage(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                testFileLink = choofdlog.FileName;
                this.pictureBox2.Image = Image.FromFile(testFileLink);
                if (!string.IsNullOrEmpty(testFileLink))
                {
                    this.button3.Enabled = true;
                }
            }
        }

        private void SelectOriginalImage(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
            {
                originalFileLink = choofdlog.FileName;
                this.pictureBox1.Image = Image.FromFile(originalFileLink);
                if (!string.IsNullOrEmpty(originalFileLink))
                {
                    this.button3.Enabled = true;
                }
            }

        }
    }
}
